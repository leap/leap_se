@title = 'Bit by Bitmask: what we\'re up to and what we\'ve thrown overboard'
@author = 'kwadronaut and some from kali'
@posted_at = '2019-07-04'
@more = true
@preview_image = '/img/pages/toolbox.jpg'

It's about time to fess up to our users: our idyllic ship that once sailed in the skies was a beautiful dream, but we all know that ships can't fly. We're now back to the basics, not just floating, but sailing. We've thrown e-mail and fat clients overboard, for now, and we're scraping rusty python 2 and Debian oldstable off our hull.

What does this mean? We're focusing hard on VPN: in the coming months we want to include [Pluggable Transports](https://2019.www.torproject.org/docs/pluggable-transports.html.en) together with [Calyx](https://www.calyxinstitute.org/). The trimmed down desktop client is a little bit more than a systray icon who handles all the hard work of configuring your VPN. [RiseupVPN](https://riseup.net/en/vpn) *is* this, obviously with their fancy bird graphics and configuration for their gateways. Bitmask as you know it in Android continues to hum along, bugs get fixed, polishing done, new releases will come. Besides that, we're experimenting with a [containerized platform](https://0xacab.org/leap/container-platform), which will make it easier to run a provider while simultaneously we'll get rid of old dependencies.

Dependencies like unmaintanable code: the javascript heavy Pixelated mail client. Or, because of the current providers engaging in planning, dropped authentication, so we don't have to cater for authenticated VPN, something that might change in the future, but currently we can drop CouchDB out of our tech stack and don't have to maintain a complex UI. This frees up a lot of brain power and makes it easier to move faster.

In short: Bitmask on your Android device, RiseupVPN to get you going on the desktop and LEAP, the platform when you want to start your own provider!

Hacking?
[RiseupVPN](https://0xacab.org/leap/riseup_vpn) is the /branded/ version of the minimal Go implementation of [Bitmask VPN](https://0xacab.org/leap/bitmask-vpn). It´s basically a wrapper around openvpn, with knowledge of what configuration files are expected to exist in a LEAP provider. The only user interface is a minimalistic systray icon that uses libappindicator.
The bitmask helper: lives for Windows and macOS in the [Bitmask VPN](https://0xacab.org/leap/bitmask-vpn/tree/master/pkg/helper/) repository. It implements a long-lived helper that runs with administrative privileges, for launching OpenVPN and the firewall. In OSX it is run as a LaunchDaemon, and in Windows we use nssm to run this helper. It communicates with Bitmask VPN via a local http service.
bitmask-root: for the snaps, in GNU/Linux we use a one-shot privileged helper that relies on policykit to elevate privileges without asking for password each time, instead of the long-lived helper that we use in osx and windows packages.

* Code & Bugtracker: https://0xacab.org/leap
* Android translations: https://www.transifex.com/otf/bitmask-android/dashboard/
* Desktop translations: https://www.transifex.com/otf/bitmask/RiseupVPN/
* Mailinglist: leap-discuss@lists.riseup.net
* Chat: ircs://irc.freenode.org/#leap if you don't have an irc client, you can use a gateway like [Matrix](https://about.riot.im/)
* ♥ donation: <https://leap.se/en/about-us/donate>


