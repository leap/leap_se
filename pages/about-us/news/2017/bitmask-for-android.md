@title = 'Bitmask for Android is back!'
@author = 'cyberta'
@posted_at = '2017-11-23'
@more = false

Our Android app arose from the mothballs!  Bug fixes, security updates, works on tablets, fixed login on 7 and supports Android 4.1 to 8. [Get it now](https://dl.bitmask.net/android/)!
