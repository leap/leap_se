@title = 'Maintenance release: Bionic packages and long term evolution'
@author = 'kwadronaut'
@posted_at = '2018-07-26'
@more = true
@preview_image = '/img/pages/bionic.png'


Update from the coagulated brains in LEAP
----------------------------------------------------------------------------------------------------

Mid july we released packages as a maintanance release  with support for Ubuntu 18.04 (Bionic). For install instructions check out https://dl.bitmask.net/linux/
The bundles however have not been updated, our plan is to deprecate them and migrate to [Snap-packages](https://en.wikipedia.org/wiki/Snappy_\(package_manager\) ), which will take some time. As usual bug reports and feedback is very welcome.

If you want to follow developments or experiment with those Snap packages, you can find an experimental one tailored for Riseup at [snapcraft](https://snapcraft.io/riseup-vpn), they've written a nice how-to as well: <https://riseup.net/betatest>

We're expanding now as well and are building a Windows installer, it's working pretty smooth, but still lacks a good firewall, we're looking forward to the near future to announce this more broadly.

* Code & Bugtracker: https://0xacab.org/leap
* Mailinglist: leap-discuss@lists.riseup.net
* Chat: ircs://irc.freenode.org/#leap if you don't have an irc client, you can use some gateway like [Matrix](https://about.riot.im/)
